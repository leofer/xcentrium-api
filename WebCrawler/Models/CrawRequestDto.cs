﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler.Models
{
    public class CrawRequestDto
    {
        private string _url;
        public string Url
        {
            get => _url.StartsWith("https://") || _url.StartsWith("http://") ? _url : $"https://{_url}";
            set => _url = value;
        }
        public int WordsToTake { get; set; }


    }
}
