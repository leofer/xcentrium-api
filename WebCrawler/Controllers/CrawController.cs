﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using WebCrawler.Models;
using WebCrawler.Services;
using System.Net.Sockets;

namespace WebCrawler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CrawController : ControllerBase
    {
        private readonly IWordsAndImageFacadeService _wordsAndImageFacadeService;

        public CrawController(IWordsAndImageFacadeService wordsAndImageFacadeService)
        {
            _wordsAndImageFacadeService = wordsAndImageFacadeService;
        }

        /// <summary>
        /// Gets the words count and all images link
        /// </summary>
        /// <param name="url"></param>
        /// <param name="wordsToTake"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<ActionResult> Post(CrawRequestDto crawRequest)
        {
            Craw res;
            try
            {
                res = await _wordsAndImageFacadeService.GetWordsAndImage(crawRequest);
            }
            catch (HttpRequestException)
            {
                return NotFound("Can not connect to requested url");
            }
            catch
            {
                return BadRequest("Unable to craw url");
            }
            return Ok(res);
        }

        




    }
}
