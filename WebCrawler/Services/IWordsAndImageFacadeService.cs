﻿using System.Threading.Tasks;
using WebCrawler.Models;

namespace WebCrawler.Services
{
    public interface IWordsAndImageFacadeService
    {
        Task<Craw> GetWordsAndImage(CrawRequestDto crawRequest);
    }
}