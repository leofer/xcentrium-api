﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebCrawler.Models;

namespace WebCrawler.Services
{
    public class WordsCrawlerService : IWordsCrawlerService
    {
        // regex to eliminate unwanted characters
        private string regex = @"(\w{2,}\.\w{2,})|\w{2,}"; // Getting only words and web addresses and emails

        public List<WordDto> GetWordsCount(HtmlNode node, int wordsToTake)
        {
            
            // getting wordsCount
            var words = GetWords(node, wordsToTake);

            return words;
        }

        private List<WordDto> GetWords(HtmlNode node, int wordsToTake)
        {
            // getting only text inside the body
            var text = node.InnerText;

            //get wordsCount
            var wordsCount = GetWordsCount(text, regex);

            //order list with number of requested words
            var orderList = wordsCount.OrderByDescending(kv => kv.Value).Take(wordsToTake).Select(kv => new WordDto
            {
                Word = kv.Key,
                Count = kv.Value
            }).ToList();

            //getting total words
            var total = wordsCount.Sum(kv => kv.Value);
            orderList.Add(new WordDto
            {
                Word = "TOTAL",
                Count = total
            });

            return orderList;

        }

        

        private Dictionary<string, int> GetWordsCount(string text, string regex)
        {
            // saves count of words
            var dict = new Dictionary<string, int>();

            // eliminating unwanted words with script
            var matches = RemoveUnwantedCharacters(text, regex);

            foreach (Match match in matches)
            {
                var word = match.Value;

                // disregard numbers
                if (word.All(char.IsDigit))
                {
                    continue;
                }

                if (!dict.ContainsKey(word))
                {
                    dict[word] = 0;
                }

                dict[word]++;
            }

            return dict;
        }

        private MatchCollection RemoveUnwantedCharacters(string text, string regex)
        {
            return Regex.Matches(text, regex);

        }
    }
}
