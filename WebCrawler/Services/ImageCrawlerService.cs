﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.ResponseCaching.Internal;

namespace WebCrawler.Services
{
    public class ImageCrawlerService : IImageCrawlerService
    {
        public List<string> GetImagesFromDocument(HtmlNode node)
        {
            var res = node.Descendants("img")
                .Select(el => el.GetAttributeValue("src", null))
                .Where(src => !string.IsNullOrEmpty(src))
                .ToList();

            return res;
        }
    }
}
