﻿using System.Collections.Generic;
using HtmlAgilityPack;
using WebCrawler.Models;

namespace WebCrawler.Services
{
    public interface IWordsCrawlerService
    {
        List<WordDto> GetWordsCount(HtmlNode node, int wordsToTake);
    }
}