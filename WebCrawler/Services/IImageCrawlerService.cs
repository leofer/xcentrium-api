﻿using System.Collections.Generic;
using HtmlAgilityPack;

namespace WebCrawler.Services
{
    public interface IImageCrawlerService
    {
        List<string> GetImagesFromDocument(HtmlNode node);
    }
}