﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using WebCrawler.Models;

namespace WebCrawler.Services
{
    public class WordsAndImageFacadeService : IWordsAndImageFacadeService
    {
        private readonly IWordsCrawlerService _wordsCrawlerService;
        private readonly IImageCrawlerService _imageCrawlerService;

        public WordsAndImageFacadeService(IWordsCrawlerService wordsCrawlerService, IImageCrawlerService imageCrawlerService)
        {
            _wordsCrawlerService = wordsCrawlerService;
            _imageCrawlerService = imageCrawlerService;
        }
        public async Task<Craw> GetWordsAndImage(CrawRequestDto crawRequest)
        {
            //var tempUrl = $"{prefix}://{url}";
            var web = new HtmlWeb();
            var doc = await web.LoadFromWebAsync(crawRequest.Url);

            // getting the body
            var htmlBody = doc.DocumentNode.SelectSingleNode("//body");

            // removing scripts from the body
            RemoveScriptFromBody(htmlBody);


            var words = _wordsCrawlerService.GetWordsCount(htmlBody, crawRequest.WordsToTake);
            var images = _imageCrawlerService.GetImagesFromDocument(htmlBody);


            var response = new Craw
            {
                WordsCount = words,
                Images = images
            };

            return response;
        }

        private void RemoveScriptFromBody(HtmlNode node)
        {
            var scriptNodes = node.SelectNodes("//script");
            foreach (var scriptNode in scriptNodes)
            {
                scriptNode.Remove();
            }
        }
    }
}
